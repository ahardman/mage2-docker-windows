# Magento 2 Docker for Apache

This basic container setup will run an nginx and PHP-FPM server environment that you can proxy requests to from Apache, which should work nicely on Windows. Just create a new `VirtualHost` in your Apache configuration pointing to `localhost:8080` and start your containers. The default configuration expects the Magento 2 files to be in `C:\xampp\htdocs\mage2`.

If you get a 500 error, make sure Apache has the `proxy_http` module enabled.

If you're using Composer, make sure to install your dependencies from inside the container:

```
docker-compose exec php-fpm composer install
```

If you're using the local PC as your MySQL server, make sure you create a user that can be used from hosts in `172.16.0.0/255.240.0.0`, and use `docker.for.win.localhost` as the hostname when setting up Magento's connection.
